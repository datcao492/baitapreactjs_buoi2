import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoeShop } from "./data";
import ProductList from "./ProductList";

export default class BTShoeShop extends Component {
  state = {
    productList: dataShoeShop,
    gioHang: [],
  };

  handleAddtoCart = (sanPham) => {
    let cloneGioHang = [...this.state.gioHang];

    let index = cloneGioHang.findIndex((item) => {
      return item.id == sanPham.id;
    });

    if (index == -1) {
      let newSanPham = { ...sanPham, soLuong: 1 };
      cloneGioHang.push(newSanPham);
    } else {
      cloneGioHang[index].soLuong++;
    }

    this.setState({ gioHang: cloneGioHang });
  };

  handleDelete = (idSanPham) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idSanPham;
    });

    if (index !== -1) {
      let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(index, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };

  handleTangGiamSoLuong = (idSanPham, giaTri) => {
    let cloneGioHang = [...this.state.gioHang];

    let index = cloneGioHang.findIndex((item) => {
      return item.id == idSanPham;
    });
    if (index !== 1) {
      cloneGioHang[index].soLuong += giaTri;
    }
    // giảm tới không thì xóa đi
    if (cloneGioHang[index].soLuong == 0) {
      cloneGioHang.splice(index, 1);
    }

    // cloneGioHang[index].soLuong==0&&cloneGioHang.splice(index,1);
    // !cloneGioHang[index].soLuong&&cloneGioHang.splice(index,1);
    this.setState({ gioHang: cloneGioHang });
  };

  handleSubTotal = (tongTien, currentValue) => {
    return tongTien + currentValue.soLuong * currentValue.price;
  };

  render() {
    return (
      <div className="d-flex">
        <div className="w-100">
          <ProductList
            productList={this.state.productList}
            handleAddtoCart={this.handleAddtoCart}
          />
        </div>
        <div className="w-100">
          <h4>Giỏ hàng: {this.state.gioHang.length}</h4>

          {this.state.gioHang.length > 0 && (
            <Cart
              gioHang={this.state.gioHang}
              handleDelete={this.handleDelete}
              handleTangGiamSoLuong={this.handleTangGiamSoLuong}
              handleSubTotal={this.handleSubTotal}
            />
          )}
        </div>
      </div>
    );
  }
}
