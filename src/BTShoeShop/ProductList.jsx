import React, { Component } from "react";
import ProductItem from "./ProductItem";

export default class ProductList extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          {this.props.productList.map((item, index) => {
            return (
              <ProductItem
                data={item}
                key={index}
                handleAddtoCart={this.props.handleAddtoCart}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
