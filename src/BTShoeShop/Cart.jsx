import React, { Component } from "react";

export default class Cart extends Component {
  render() {
    let subTotal = this.props.gioHang.reduce(this.props.handleSubTotal, 0);
    return (
      <div className="container">
        <table className="table">
          <thead>
            <td>Tên</td>
            <td>Image</td>
            <td>Giá</td>
            <td>Số lượng</td>
            <td>Tổng</td>
          </thead>
          <tbody>
            {this.props.gioHang.map((item) => {
              return (
                <tr>
                  <td>{item.name}</td>
                  <td className="image2 p-0 m-0">
                    <img
                      style={{
                        width: "70px",
                        height: "70px",
                        backgroundPosition: "center",
                        backgroundSize: "cover",
                        backgroundRepeat: "no-repeat",
                      }}
                      src={item.image}
                      alt=""
                    />
                  </td>
                  <td>$ {item.price}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleTangGiamSoLuong(item.id, -1);
                      }}
                      className="btn btn-danger "
                    >
                      -
                    </button>
                    {item.soLuong}
                    <button
                      onClick={() => {
                        this.props.handleTangGiamSoLuong(item.id, 1);
                      }}
                      className="btn btn-success"
                    >
                      +
                    </button>
                  </td>
                  <td className="text-success">
                    $ {`${item.price}` * `${item.soLuong}`}
                  </td>
                  <td>
                    <button
                      className="btn btn-danger"
                      onClick={() => {
                        this.props.handleDelete(item.id);
                      }}
                    >
                      <i className="fa fa-trash-alt"></i>
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
          <tfoot>
            <td></td>
            <td></td>
            <td></td>
            <td className="font-weight">SUBTOTAL:</td>
            <td className="font-weight">$ {subTotal}</td>
          </tfoot>
        </table>
      </div>
    );
  }
}
