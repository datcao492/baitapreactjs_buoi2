import React, { Component } from "react";

export default class ProductItem extends Component {
  render() {
    let { name, price, image } = this.props.data;
    return (
      <div className="card col-3">
        <img src={image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title text-center">{name}</h5>
        </div>
        <p className="card-text text-center text-danger">${price}</p>
        <button
          onClick={() => {
            this.props.handleAddtoCart(this.props.data);
          }}
          className="btn btn-warning"
        >
          Add to cart
        </button>
      </div>
    );
  }
}
